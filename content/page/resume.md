---
title: Resume
subtitle: Why you'd want to work with me
comments: false
---

# Aaron Di Silvestro

aaron (at) bkrn.org

https://bkrn.gitlab.io/page/resume/

## Drives

+ I want to constantly solve hard problems with brilliant people
+ I thrive with fact driven argument and radical honesty
+ I need my work to leave the world better than it started
+ I use spaces but you can use tabs, my editor will clean it up

## Projects

[pad](https://pad.bkrn.org/) is a simple note taking application I wrote because I wanted a simple note taking app and I couldn't be bothered to do a Google search for one. It is Elm+Go and is deployed in elastic beanstalk because I was curious why people use elastic beanstalk and then I never moved it.

[exceed](https://gitlab.com/bkrn/exceed) is my occasional attempt to make a spreadsheet application that allows multiple clients on a file system based store. I am constantly inspired because my analysts keep balking at web based solutions to collaborative excel. It's honestly more drawings than code.

[guide](https://gitlab.com/bkrn/guide) is an exploration of WASM based around a recurring dream I have of making a snowpack simulator for avalanche education. No readme in there but `npm run serve` should get you some pretty useless webgl fun.

## What I do now

**Full Stack Engineer @ Nike Inc.**
*2015 - Present*

I'm a software engineer on Nike's sustainability advanced analytics team within the Sustainable Business and Innovation organization.

While there I've transitioned from a data heavy analyst/engineer hybrid to a full fledged application developer leading/working with small teams of engineers (2-3)  developing tools that integrate sustainability analytics into the enterprise as a whole.

I'm most proud of a ground up implementation of a sustainability scenario planning tool that integrates with Nike data systems and provides sustainability analysts a UI to model future versions of Nike to drive action and planning through the generated reports.

I directed the work of multiple engineers working on several front end user interfaces in the project and fully wrote the service that did the data integration and modeling, I also designed and implemented the DSL that allows analysts to create reusable scenarios, which was a blast.

## Skills I have

### Languages I use at work

+ Go
	+ This is my principal work language, I have architected (sp?), written, deployed, and maintained multiple services in go
+ Rust
	+ Growing in importance for me, I have written two DSLs interpreted in Rust and have written performance critical code in Rust
+ Python
	+ My scripting language, ETL's ad hoc data analysis and tasks that are one step over my bash/sh skills
+ R
	+ I prefer python but have to work with data analyst's R code more than infrequently
+ Elm
	+ I'm not a front end guy so I reach for Elm's guarantees when I need to write user facing web apps
+ JS/TS - mostly D3
	+ Can hack away in vanilla JS and muddle through JQuery if I need to. When I need to write a [visualization](https://sustainability.nike.com/sunburst/index.html) I tend to reach for typescript because I like the structure
+ SQL
	+ Handle JSON and CTEs in Postgres, can poke my way through performance concerns and mostly not shoot myself in the foot in DB heavy services. I think that ORMs are icky
+ Shell with heavy bash leaning
	+ There's ~probably~ better tools out there but I tend to do my one step builds and integration testing in bash first
+ English
	+ I really like talking and listening but have forgotten my high school and college French classes

### Tools/Techs I work with
+ Postgres, Oracle, SQL Server
+ Mongo, Dynamo (A little), Rethink
	+ I tend to push back against jumping right to no-sql at this point, but I use it where I need it, knowing what I'm losing
+ Lambda, ECS
	+ Not totally sold on serverless but I'm not devOps and they say they like it
+ Docker
+ Git

### Stuff I use for fun
+ WASM (From Rust)
	+ It'll be like node.js but from the back end, I look forward to ruining the web when JS interop doesn't take forever. I've done a few pet projects with it.
+ That arduino version of C++
	+ Started more projects than I've finished, obviously
+ Haskell
	+ Learning Elm well  helped and I've been tinkering against Project Euler with Haskell but I need to dedicate more time to feel competent
+ Phoenix/Elixer
	+ Haven't found a place where it would be a responsible call at work but I sure do like it
	
### Stuff I have done

I went to law school (Lewis and Clark for three years), got an undergraduate degree in political science (George Washington), worked as an office manger/testing proctor (Living in Bend, OR - which is amazing), and did a short stint as a data analyst (at Cambia Health Systems in Portland, OR). Finally, I realized that I wanted to be a software engineer while doing sports analytics blogging.
